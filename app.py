from flask import Flask, request, jsonify, redirect, url_for
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from flask_migrate import Migrate
from flask import render_template
from markupsafe import Markup
from sqlalchemy.exc import IntegrityError



import os


app = Flask(__name__)
basedir = os.path.abspath(os.path.dirname(__file__))

#Database
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(basedir, 'db.sqlite')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

#init db
db = SQLAlchemy(app)
#init ma
ma = Marshmallow(app)
#migrations
migrate = Migrate(app, db)


class Topic(db.Model):
    id = db.Column(db.Integer, primary_key = True)
    name = db.Column(db.String(100))

    def __init__(self, name):
        self.name = name
        
class Keyterm(db.Model):
    id = db.Column(db.Integer, primary_key = True)
    name = db.Column(db.String(100))
    topic_id = db.Column(db.Integer, db.ForeignKey('topic.id'))
    topic = db.relationship("Topic", backref='keyterms')


    
    def __init__(self, name, topic):
        self.name = name
        self.topic_id = topic

#db schema
class ProductSchema(ma.Schema):
    class Meta:
        fields = ('id', 'topic', 'keyword')
    
class TopicSchema(ma.Schema):
    class Meta:
        fields = ('id', 'name')
        model = Topic

    
class KeytermSchema(ma.Schema):
    class Meta:
        model = Keyterm
        fields = ('id', 'name', 'topic_id')

#init schema
topic_schema = TopicSchema()
topics_schema = TopicSchema(many=True)

keyterm_schema = KeytermSchema()
keyterms_schema = KeytermSchema(many=True)

if __name__ == '__main__':
    app.run(debug=True)

#get all topics
@app.route('/topic', methods=['GET'])
def getAllTopicsFromDb():
    allTopics = Topic.query.all()
    result = topics_schema.dump(allTopics)
    return jsonify(result)


#get single topic
@app.route('/topic/<id>', methods=['GET'])
def getTopic(id):
    # topic = Topic.query.get(id)
    topic = Topic.query.filter_by(id=id).first()
    output = topic_schema.dump(topic)
    if(len(output) == 0):
        return jsonify({"message":"Topic doesn't exist"})
    else:
        return topic_schema.jsonify(topic)

#get keyterms for a topic
@app.route('/keyterms/<topic_id>', methods=['GET'])
def getKeyterm(topic_id):
    allKeyterms = Keyterm.query.filter_by(topic_id = topic_id).all()
    output = keyterms_schema.dump(allKeyterms)
    if(len(output) == 0):
        return jsonify({"message":"Keyterm doesn't exist"})
    else:
        return jsonify({"Keyterms for id " + topic_id: output})
   

#create topic
@app.route('/createTopic', methods=['GET'])
def addTopic():
    # topic = request.json['name']
    topic = request.args['name']
    new_topic = Topic(topic)
    db.session.add(new_topic)
    db.session.commit()
    return topic_schema.jsonify(new_topic)

#create keyterm
@app.route('/createKeyterm', methods=['GET'])
def addKeyterm():

    topic_id = request.args['topicid']
    keyterm = request.args['name']
    new_keyterm = Keyterm(keyterm, topic_id)

    db.session.add(new_keyterm)
    db.session.commit()
    return topic_schema.jsonify(new_keyterm)


# get all keyterms
# @app.route('/keyterms', methods=['GET'])
# def getAllKeyterms():
#     allKeyterms = Keyterm.query.all()
#     result = keyterms_schema.dump(allKeyterms)
#     return jsonify(result)

# #update keywords for a topic
# @app.route('/createKeyword/<id>', methods=['PUT'])
# def updateTopic(id):
#     existingTopic = Product.query.get(id)
#     keyword = request.json['keyword']

#     existingTopic.keyword = keyword

#     db.session.commit()

    # return product_schema.jsonify(existingTopic)

    #create keyword for topic
# @app.route('/createKeyword/<topic>', methods=['POST'])
# def addKeyword(topic):
#     keyword = request.json['keyword']
#     new_product = Product(topic, keyword)

#     db.session.add(new_product)
#     db.session.commit()

#     return product_schema.jsonify(new_product)


# #delete topic
# @app.route('/deleteTopic/<id>', methods=['DELETE'])
# def deleteTopic(id):
#     topic = Product.query.get(id)
#     db.session.delete(topic)
#     db.session.commit()
    
#     return product_schema.jsonify(topic)